import 'dotenv/config';

import app from './app/app';

const { PORT = 4000 } = process.env;

const startServer = async (): Promise<void> => {
    const startServerMessage = `Server started at port: ${PORT}`;

    app.listen(PORT, () => console.log(startServerMessage));
};

startServer();
