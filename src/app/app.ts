import express, { Request, Response } from 'express';

import cors from 'cors';

import getTimestamp from './utilities/get-timestamp.utility';

const app = express();

app.use(cors());

app.get('/', (_: Request, res: Response) => {
    res.status(200).json({
        message: 'Current timestamp',
        timestamp: getTimestamp(),
    });
});

export default app;
