import getTimestamp from './get-timestamp.utility';

describe('getTimestamp', () => {
    it('should return the current timestamp', () => {
        const timestamp = getTimestamp();

        expect(typeof timestamp).toBe('number');

        expect(timestamp).toBeGreaterThan(0);
    });
});
