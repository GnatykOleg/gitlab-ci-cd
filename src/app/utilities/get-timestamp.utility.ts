const getTimestamp = (): number => new Date().getTime();

export default getTimestamp;
