FROM node:18-alpine as base

WORKDIR /app

FROM base as build

COPY package*.json .

RUN npm ci 

COPY . .

RUN npm run build

FROM base as production

COPY --from=build /app/build ./build

COPY package*.json .

RUN npm install --production

CMD npm run start